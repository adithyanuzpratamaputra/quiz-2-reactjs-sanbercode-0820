// soal 14
console.log("-Soal Nomer 14-")
console.log()

const Balok = (...args) => {
    let sum = 1;
    for (let arg of args) sum = sum * arg;
    return sum;
}

const Kubus = (...args) => {
    let sum = 1;
    for (let arg of args) sum = sum * arg;
    return sum;
}

console.log( "Balok = "+Balok(3, 6, 9) );
console.log( "Kubus = "+Kubus(3, 3, 3) );